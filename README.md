# Utah Legislature API Client
This is a simple Java client to interact with the Utah State Legislature's public API, which can be found here: https://le.utah.gov/data/developer.htm

# Local setup
In order to run the tests, you need to create an `application-local.yml` file in `src/test/resources`. You can copy the existing `application-local_sample.yml` file.

The `secret-token` property can be found by logging in to https://le.utah.gov/tracking/tracking.jsp and clicking the "Developer" tab.

# Learning
* https://spring.io/guides/gs/multi-module/
* https://www.baeldung.com/spring-5-webclient
